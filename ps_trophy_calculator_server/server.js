require("dotenv").config();
let express = require("express");
let app = express();
let HTTP_PORT = process.env.PORT || 5000

app.use(express.static("public"));

app.get('/', (req, res) => {
    res.send('<h1>PlayStation Trophy Calculator Server</h1>')
});

app.listen(HTTP_PORT, () => {
    console.log("Server starting on port: "+ HTTP_PORT)
});
