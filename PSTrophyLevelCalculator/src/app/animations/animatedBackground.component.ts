import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'animated_background',
  templateUrl: './animatedBackground.component.html',
  styleUrls: ['../app.component.scss'],
})
export class AnimatedBackgroundComponent implements OnInit {
  @Input() animationChanceDenominator: number = 10;

  symbols: any = [
    '../../assets/images/ps-cross.svg',
    '../../assets/images/ps-square.svg',
    '../../assets/images/ps-triangle.svg',
    '../../assets/images/ps-circle.svg',
  ];
  symbolsAnimationInfo: any = [];
  symbolNum: number = 1000;
  symbolIndex: number = 0;

  createSymbolAnimationInfo() {
    for (let i = 0; i < this.symbolNum; i++) {
      this.symbolsAnimationInfo.push({
        image: this.symbols[this.symbolIndex],
        animate: this.randomizeAnimation(),
      });
      if (this.symbolIndex == 3) {
        this.symbolIndex = 0;
      } else {
        this.symbolIndex++;
      }
    }
  }

  randomizeAnimation() {
    let minNum = 1;
    let maxNum = this.animationChanceDenominator;

    let randomNum = Math.floor(Math.random() * (maxNum - minNum) + minNum);

    return randomNum == 1;
  }

  ngOnInit(): void {
    this.createSymbolAnimationInfo();
  }
}
