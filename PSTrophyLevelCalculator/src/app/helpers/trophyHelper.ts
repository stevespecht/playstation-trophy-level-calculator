export class TrophyHelper {
  private totalTrophyCount: number = 0;
  private totalTrophyPoints: number = 0;
  private trophyLevel: number = 0;
  private percentToNextLevel: number = 0;

  /**
   * @desc calculates the total trophy points, total trophy
   * @param platinumCount : number - number of platinum trophies
   * @param goldCount : number - number of gold trophies
   * @param silverCount : number - number of silver trophies
   * @param bronzeCount : number - number of bronze trophies
   * @return object - trophyCount, trophyPoints, trophyLevel
   **/
  public calculateTrophyInfo(
    platinumCount: number,
    goldCount: number,
    silverCount: number,
    bronzeCount: number
  ) {
    this.totalTrophyCount = 0;
    this.totalTrophyPoints = 0;
    this.trophyLevel = 0;
    this.percentToNextLevel = 0;

    this.calculateTrophyPoints(
      platinumCount,
      goldCount,
      silverCount,
      bronzeCount
    );

    this.calculateTrophyLevel(this.totalTrophyPoints);

    return {
      trophyCount: this.totalTrophyCount,
      trophyPoints: this.totalTrophyPoints,
      trophyLevel: this.trophyLevel,
      percentToNextLevel: this.percentToNextLevel,
    };
  }

  /**
   * @desc calculates the total trophy points based of the trophy values
   * @param platinumCount : number - number of platinum trophies
   * @param goldCount : number - number of gold trophies
   * @param silverCount : number - number of silver trophies
   * @param bronzeCount : number - number of bronze trophies
   * @return: number - total trophy points
   **/
  private calculateTrophyPoints(
    platinumCount: number,
    goldCount: number,
    silverCount: number,
    bronzeCount: number
  ) {
    const PLATINUM_VALUE = 180;
    const GOLD_VALUE = 90;
    const SILVER_VALUE = 30;
    const BRONZE_VALUE = 15;

    this.totalTrophyCount +=
      platinumCount + goldCount + silverCount + bronzeCount;

    this.totalTrophyPoints += platinumCount * PLATINUM_VALUE;
    this.totalTrophyPoints += goldCount * GOLD_VALUE;
    this.totalTrophyPoints += silverCount * SILVER_VALUE;
    this.totalTrophyPoints += bronzeCount * BRONZE_VALUE;
  }

  /**
   * @desc calculates the trophy level based of the total trophy points, and the percentage to next trophy level
   * @param totalTrophyPoints: number - total trophy points
   * @return number - trophy level
   **/
  private calculateTrophyLevel(totalTrophyPoints: number) {
    let trophyLevelScale = [
      { level: 1, points: 0 },
      { level: 2, points: 200 },
      { level: 3, points: 600 },
      { level: 4, points: 1200 },
      { level: 5, points: 2400 },
      { level: 6, points: 4000 },
      { level: 7, points: 6000 },
      { level: 8, points: 8000 },
      { level: 9, points: 10000 },
      { level: 10, points: 12000 },
      { level: 11, points: 14000 },
      { level: 12, points: 16000 },
      { level: 13, points: 24000 },
    ];

    const TROPHY_POINT_INCREMENTS_8k = 8000;
    const TROPHY_POINT_INCREMENT_10k = 10000;
    let trophyLevelThreshold = 24000;

    //build trophy level scale
    for (let i = 14; i <= 26; i++) {
      trophyLevelThreshold += TROPHY_POINT_INCREMENTS_8k;
      trophyLevelScale.push({ level: i, points: trophyLevelThreshold });
    }

    for (let i = 27; i <= 500; i++) {
      trophyLevelThreshold += TROPHY_POINT_INCREMENT_10k;
      trophyLevelScale.push({ level: i, points: trophyLevelThreshold });
    }

    //determine trophy level
    for (let i = 0; i < trophyLevelScale.length; i++) {
      if (
        totalTrophyPoints > trophyLevelScale[i].points &&
        totalTrophyPoints < trophyLevelScale[i + 1].points
      ) {
        this.trophyLevel = trophyLevelScale[i].level;

        //calculate percentage to next level
        let percentToNextLevelUnrounded =
          ((totalTrophyPoints - trophyLevelScale[i].points) /
            (trophyLevelScale[i + 1].points - trophyLevelScale[i].points)) *
          100;

        //round percentage to 2 decimal places
        this.percentToNextLevel =
          Math.round(percentToNextLevelUnrounded * 100) / 100;

        break;
      }
    }
  }
}
