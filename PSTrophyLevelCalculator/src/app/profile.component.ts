import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators,
} from '@angular/forms';

import { ValidateNumber } from './validators/number.validator';
import { TrophyHelper } from './helpers/trophyHelper';

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./app.component.scss'],
})
export class ProfileComponent implements OnInit {
  @Input() profileIndex: number;
  @Input() profileCount: number;
  @Output() removeProfileEmitter = new EventEmitter();
  @Output() profileInfoEmitter = new EventEmitter();
  @Output() isProfileValidEmitter = new EventEmitter();

  trophyTypes: string[] = ['Platinum', 'Gold', 'Silver', 'Bronze'];

  profileForm: FormGroup;
  numPlatinumTrophies: FormControl;
  numGoldTrophies: FormControl;
  numSilverTrophies: FormControl;
  numBronzeTrophies: FormControl;

  totalNumTrophies: number = 0;
  totalTrophyPoints: number = 0;
  trophyLevel: number = 0;
  percentToNextLevel: number = 0;
  nextTrophyLevel: number = 0;

  displayTrophyLevel: boolean = false;

  constructor(private builder: FormBuilder) {
    this.numPlatinumTrophies = new FormControl(
      '',
      Validators.compose([Validators.required, ValidateNumber])
    );
    this.numGoldTrophies = new FormControl(
      '',
      Validators.compose([Validators.required, ValidateNumber])
    );
    this.numSilverTrophies = new FormControl(
      '',
      Validators.compose([Validators.required, ValidateNumber])
    );
    this.numBronzeTrophies = new FormControl(
      '',
      Validators.compose([Validators.required, ValidateNumber])
    );
  }

  ngOnInit() {
    this.profileForm = this.builder.group({
      numPlatinumTrophies: this.numPlatinumTrophies,
      numGoldTrophies: this.numGoldTrophies,
      numSilverTrophies: this.numSilverTrophies,
      numBronzeTrophies: this.numBronzeTrophies,
    });
  }

  /**
   * @desc evaluates the trophy counts to display
   * the total trophy count, total trophy points and trophy level for this profile
   * emits the trophy counts to the parent for combined profile totals
   * @param N/A
   * @return N/A
   **/
  calculateProfileInfo() {
    let platinumCount = 0;
    let goldCount = 0;
    let silverCount = 0;
    let bronzeCount = 0;

    //get trophy counts
    platinumCount = this.profileForm.get('numPlatinumTrophies').value;
    goldCount = this.profileForm.get('numGoldTrophies').value;
    silverCount = this.profileForm.get('numSilverTrophies').value;
    bronzeCount = this.profileForm.get('numBronzeTrophies').value;

    //calculate trophy info
    let trophyHelper = new TrophyHelper();

    let trophyInfo = trophyHelper.calculateTrophyInfo(
      platinumCount,
      goldCount,
      silverCount,
      bronzeCount
    );

    //assign calculated trophy info to binding variables
    this.totalNumTrophies = trophyInfo.trophyCount;
    this.totalTrophyPoints = trophyInfo.trophyPoints;
    this.trophyLevel = trophyInfo.trophyLevel;
    this.percentToNextLevel = trophyInfo.percentToNextLevel;
    this.nextTrophyLevel = trophyInfo.trophyLevel + 1;

    this.displayTrophyLevel = true;

    //emit profile trophy info to parent
    this.profileInfoEmitter.emit({
      index: this.profileIndex,
      platinum: platinumCount,
      gold: goldCount,
      silver: silverCount,
      bronze: bronzeCount,
    });
  }

  /**
   * @desc clear profile form of all inputted trophy counts
   * @param N/A
   * @return N/A
   **/
  clearForm() {
    this.profileForm.reset();
    this.displayTrophyLevel = false;
  }

  notifyParentOfValidity() {
    this.isProfileValidEmitter.emit({
      index: this.profileIndex,
      validity: this.profileForm.valid,
    });
  }
}
