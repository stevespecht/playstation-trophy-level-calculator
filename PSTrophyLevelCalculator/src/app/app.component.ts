import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';

import { ProfileComponent } from './profile.component';
import { TrophyHelper } from './helpers/trophyHelper';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  @ViewChildren(ProfileComponent)
  profileChildren: QueryList<ProfileComponent>;

  profiles: any = [{ platinum: 0, gold: 0, silver: 0, bronze: 0 }];

  totalNumTrophies: number = 0;
  totalTrophyPoints: number = 0;
  trophyLevel: number = 0;
  percentToNextLevel: number = 0;
  nextTrophyLevel: number = 0;

  displayTrophyLevel: boolean = false;
  disableAddProfileButton: boolean = false;
  disableCalculateButton: boolean = true;

  ngOnInit() {}

  /**
   * @desc adds a new profile to the profiles array
   * @param N/A
   * @return N/A
   **/
  addProfile() {
    this.profiles.push({
      platinum: 0,
      gold: 0,
      silver: 0,
      bronze: 0,
      validity: false,
      updated: false,
    });

    this.disableCalculateButton = true;
  }

  /**
   * @desc removes a profile from the profiles array based off profile index
   * @param profileIndex: number - index of the profile to be removed
   * @return N/A
   **/
  removeProfile(profileIndex: number) {
    this.profiles.splice(profileIndex, 1);
  }

  /**
   * @desc notifies all profile children to calculate their own trophy level
   * and emit their trophy counts to the parent
   * @param N/A
   * @return N/A
   **/
  notifyChildrenToCalculate() {
    this.profileChildren.forEach((child) => child.calculateProfileInfo());

    this.disableAddProfileButton = true;
  }

  /**
   * @desc collects the profile info from all profiles emitting their trophy counts
   * calculates the total trophy count, total trophy points and trophy level if all accounts where combined
   * @param profileTrophyCounts: any - object containing trophy counts for a profile
   * * @return N/A
   **/
  collectProfileInfo(profileTrophyCounts: any) {
    //update profile object
    this.profiles[profileTrophyCounts.index].platinum =
      profileTrophyCounts.platinum;
    this.profiles[profileTrophyCounts.index].gold = profileTrophyCounts.silver;
    this.profiles[profileTrophyCounts.index].silver =
      profileTrophyCounts.silver;
    this.profiles[profileTrophyCounts.index].bronze =
      profileTrophyCounts.bronze;
    this.profiles[profileTrophyCounts.index].updated = true;

    //only combine profiles if all profiles have been updated
    for (let i = 0; i < this.profiles.length; i++) {
      if (!this.profiles[i].updated) {
        break;
      }

      if (this.profiles.length > 1) {
        this.combineProfiles();
      }
    }
  }

  combineProfiles() {
    let allProfilePlatinum = 0;
    let allProfileGold = 0;
    let allProfileSilver = 0;
    let allProfileBronze = 0;

    //add up all trophy counts
    this.profiles.forEach((profile) => {
      allProfilePlatinum += profile.platinum;
      allProfileGold += profile.gold;
      allProfileSilver += profile.silver;
      allProfileBronze += profile.bronze;
    });

    //calculate trophy info
    let trophyHelper = new TrophyHelper();

    let allProfileTrophyInfo = trophyHelper.calculateTrophyInfo(
      allProfilePlatinum,
      allProfileGold,
      allProfileSilver,
      allProfileBronze
    );

    //assign calculated trophy info to binding variables
    this.totalNumTrophies = allProfileTrophyInfo.trophyCount;
    this.totalTrophyPoints = allProfileTrophyInfo.trophyPoints;
    this.trophyLevel = allProfileTrophyInfo.trophyLevel;
    this.percentToNextLevel = allProfileTrophyInfo.percentToNextLevel;
    this.nextTrophyLevel = allProfileTrophyInfo.trophyLevel + 1;

    this.displayTrophyLevel = true;
  }

  clearProfiles() {
    this.totalNumTrophies = 0;
    this.totalTrophyPoints = 0;
    this.trophyLevel = 0;
    this.displayTrophyLevel = false;
    this.disableCalculateButton = true;
    this.disableAddProfileButton = false;

    this.profileChildren.forEach((child) => child.clearForm());
  }

  validateProfiles(profileValidityInfo: any) {
    //update profiles validity
    this.profiles[profileValidityInfo.index].validity =
      profileValidityInfo.validity;

    //validate all profiles
    for (let i = 0; i < this.profiles.length; i++) {
      if (!this.profiles[i].validity) {
        this.disableCalculateButton = true;
        break;
      }

      this.disableCalculateButton = false;
    }
  }
}
