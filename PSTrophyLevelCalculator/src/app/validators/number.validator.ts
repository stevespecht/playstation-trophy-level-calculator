import { AbstractControl } from '@angular/forms';
export function ValidateNumber(control: AbstractControl) {
  const NUMBER_REGEXP = /^\d*$/;
  return !NUMBER_REGEXP.test(control.value) ? { invalidNumber: true } : null;
} // ValidateNumber
